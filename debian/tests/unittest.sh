#!/bin/sh

set -eu

# Disable proxies from build system
export http_proxy=
export no_proxy=

python3 run_tests.py
